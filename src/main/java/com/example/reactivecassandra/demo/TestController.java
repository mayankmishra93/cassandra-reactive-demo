package com.example.reactivecassandra.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/demo")
@RestController
public class TestController {
	
	@Autowired private PersistentService persistentService;
	
	@GetMapping(value = "/save")
	  @ResponseStatus(HttpStatus.OK)
	  String get() {
		Person x = new Person();
	  	x.setFirstname("Mayank1");
	  	x.setLastname("Mishra2");
	  	x.setAge(30);
	  	persistentService.save(x).block();
	  	return null;
	}
}
