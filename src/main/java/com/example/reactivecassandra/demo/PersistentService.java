package com.example.reactivecassandra.demo;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersistentService extends ReactiveCassandraRepository<Person, String>{

}
