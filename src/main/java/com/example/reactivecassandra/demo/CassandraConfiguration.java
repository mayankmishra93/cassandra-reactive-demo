package com.example.reactivecassandra.demo;

import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.session.DefaultBridgedReactiveSession;
import reactor.core.scheduler.Schedulers;
import org.springframework.data.cassandra.ReactiveSession;
import org.springframework.data.cassandra.config.AbstractReactiveCassandraConfiguration;


@Configuration
public class CassandraConfiguration extends AbstractReactiveCassandraConfiguration {
 
	@Override
	protected String getKeyspaceName() {
		return "test123";
	}

	@Override
	public SchemaAction getSchemaAction() {
		return SchemaAction.RECREATE;
	}

}